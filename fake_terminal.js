let fake_terminal = {
    consoleAttributs : {
        helper : true,
        widthSize : 80,
        heightSize : 500,
        theme : {
            dark : {
                background : "rgba(0, 0, 0, 0.746)",
                text_commands : "rgb(255, 255, 255)",
                text_user : "rgb(150, 231, 28)",
            },
        },
        appendTo : "body",
        user : "fakeuser",
        hostname : "fakehost",
        fakePath : "/",
        role : "",

    },
    datas : {
        "fakeTreePath" : {
            "root" : {
                "home" : {
                    "Seb" : {
                        
                    },
                    "user" : {

                    },
                },
            },
        },
        "fakeFiles" : {

        },
    },
    setFakeData : function(datas){
        datas = JSON.stringify(datas);
        localStorage.setItem("fake_terminal_datas" , datas);
        this.datas = datas;
    },
    getFakeDatas : function(){
        let datas = JSON.parse(localStorage.fake_terminal_datas);
        this.datas = datas;
        return datas;
    },
    insertConsole : function(){
        if(typeof localStorage.fake_terminal_datas == "undefined"){
            this.setFakeData(this.datas);
        } else {
            this.getFakeDatas();
        }

        let margin = (100 - this.consoleAttributs.widthSize)/2;
        let html = `<section id="section_fake_terminal" style="
                        overflow: scroll;
                        padding: 1%;
                        margin-left: ${margin}%;
                        margin-right: ${margin}%;
                        width: ${this.consoleAttributs.widthSize}%;
                        height: ${this.consoleAttributs.heightSize}px;
                        background-color: ${this.consoleAttributs.theme.dark.background};
                        color: ${this.consoleAttributs.theme.dark.text_user};">
                    </section>`;
        $(this.consoleAttributs.appendTo).append(html);
        this.insertHelper();
        this.insertPrompt();
        $("#spanEditable_fake_terminal").focus();
        $("#section_fake_terminal").on("click", function(){
            $("#spanEditable_fake_terminal").focus();
        });
    },
    insertPrompt : function(){
        let role = this.consoleAttributs.role == "root" ? "#" : "$";
        let html = `<div id="prompt_fake_terminal">
                        ${this.consoleAttributs.user}@${this.consoleAttributs.hostname}:${this.consoleAttributs.fakePath}${role}
                        <span style="color: ${this.consoleAttributs.theme.dark.text_commands};" id="spanEditable_fake_terminal" contenteditable="true">
                        <span>
                    </div>`;
        $("#section_fake_terminal").append(html);
        $("#spanEditable_fake_terminal").on("keyup", function(key){
            if(key.which == 13){
                let command = $(this).text().trim();
                $(this).text("");
                fake_terminal.callFunction(command);
            }
        });
        $("#spanEditable_fake_terminal").focus();
    },
    insertFakePrompt : function(command){
        let html = `<div>
                        ${this.consoleAttributs.user}@${this.consoleAttributs.hostname}:${this.consoleAttributs.fakePath}$
                        <span style="color: ${this.consoleAttributs.theme.dark.text_commands};">
                            ${command}
                        <span>
                    </div>`;
        $("#section_fake_terminal").append(html);
    },
    insertHelper : function(){
        if(this.consoleAttributs.helper){
            let html = `<p style="color : white;">Thanks for installing fake_terminal and welkome to helper.<br>
                        The object fake_terminal is yet available.<br>
                        Check fake_terminal.consoleAttributs array for settings.<br>
                        How to make my custom function?<br>
                        In js personal script:<br>
                        fake_terminal["nameFunction"] = function(){<br>
                        let command = arrayCommand.toString().replace(/,/g," ");
                        this.insertFakePrompt(command);<br>
                        ...code...<br>
                        this.insertResponse(string nameCommand, string responseForTerminal);<br>
                        ...other responses...<br>
                        this.insertPrompt();<br>
                        }
                        </p>`;
            $("#section_fake_terminal").append(html);
        }
    },
    callFunction : function(command){
        arrayCommand = command.split(" ");
        if(typeof this[arrayCommand[0]] != "undefined"){
            this[arrayCommand[0]](arrayCommand);
        } else if(arrayCommand[0] == ""){

        } else {
            let response = arrayCommand[0]+" : commande introuvable";
            this.insertFakePrompt(command);            
            this.insertResponse(response);
            this.insertPrompt();
        }
        this.setFakeData(this.datas);
    },
    insertResponse : function(response){
        $("#prompt_fake_terminal").remove();
        let html = `<p style="color : rgb(255, 255, 255)">${response}</p>`;
        $("#section_fake_terminal").append(html);        
    },
    getPathAndFiles : function(arg){
        let fakePath = "this.datas.fakeTreePath";

        let pathArray = [];
        if(arg == "/"){
            pathArray = ["root"]
        } else {
            let pathArray = arg.split("/" );
        }

        for (const index in pathArray){
            switch (pathArray[index]) {
                case "~":
                    pathArray[index] = "home.user";
                    break;
                case this.consoleAttributs.user:
                    pathArray[index] = "user";
                    break;
            }

            fakePath += "."+pathArray[index];
        }
        console.log(fakePath);

        return fakePath;
    },
    clear : function(arrayCommand){
        $("#section_fake_terminal").html("");
        this.insertPrompt();
    },
    ls : function(arrayCommand){
        let command = arrayCommand.toString().replace(/,/g," ");
        this.insertFakePrompt(command);

        if(typeof arrayCommand[1] == "undefined"){
            arrayCommand[1] = this.consoleAttributs.fakePath;
        }

        let text = this.getPathAndFiles(arrayCommand[1]);

        this.insertResponse(text);
        this.insertPrompt();
    },
}