$().ready(()=>{
    fake_terminal.consoleAttributs.hostname = "busschot.fr";
    fake_terminal.consoleAttributs.user = "Seb";
    fake_terminal.consoleAttributs.helper = false;
    fake_terminal["test"] = function(arrayCommand){
        let command = arrayCommand.toString().replace(/,/g," ");
        this.insertFakePrompt(command);
        this.insertResponse("It's work !");
        this.insertPrompt();
    };
    fake_terminal.insertConsole();
});